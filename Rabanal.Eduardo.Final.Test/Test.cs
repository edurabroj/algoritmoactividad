﻿using NUnit.Framework;
using Rabanal.Eduardo.Final.Algoritmo;
using Rabanal.Eduardo.Final.AlgoritmoModelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rabanal.Eduardo.Final.Test
{
    [TestFixture]
    public class Test
    {
        AlgoritmoActividad algoritmo;
        [SetUp]
        public void SetUp()
        {
            algoritmo = new AlgoritmoActividad();
        }

        [Test]
        public void Prueba1()
        {
            Assert.AreEqual 
            ("Lavar la ropa, desde enero hasta agosto, todos los sábados, semanal.",
                algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Lavar la ropa",
                        MesDesde=1,
                        MesHasta=8,
                        Frecuencia=Frecuencia.semanal,
                        Dia=6
                    }
                )
            );
            Assert.True(algoritmo.EsValido);
            Assert.True(algoritmo.Errores == "");
        }
        [Test]
        public void Prueba2()
        {
            Assert.AreEqual
            ("Pagar el recibo de luz, desde enero hasta diciembre, todos los fin de mes, mensual.",
                algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Pagar el recibo de luz",
                        MesDesde = 1,
                        MesHasta = 12,
                        Frecuencia = Frecuencia.mensual,
                        Dia = 31
                    }
                )
            );
            Assert.True(algoritmo.EsValido);
            Assert.True(algoritmo.Errores == "");
        }
        [Test]
        public void Prueba3()
        {
            Assert.AreEqual
            ("Enviar Dinero a Miguel, desde enero hasta diciembre, cualquier día, semanal.",
                algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Enviar Dinero a Miguel",
                        MesDesde = 1,
                        MesHasta = 12,
                        Frecuencia = Frecuencia.semanal
                    }
                )
            );
            Assert.True(algoritmo.EsValido);
            Assert.True(algoritmo.Errores == "");
        }
        [Test]
        public void Prueba4()
        {
            Assert.AreEqual
            ("Pagar recibo de agua, en enero, todos los 27, mensual.",
                algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Pagar recibo de agua",
                        MesDesde = 1,
                        MesHasta = 1,
                        Frecuencia = Frecuencia.mensual,
                        Dia=27
                    }
                )
            );
            Assert.True(algoritmo.EsValido);
            Assert.True(algoritmo.Errores == "");
        }
        [Test]
        public void Prueba5()
        {
            Assert.AreEqual
            ("Ir a correr, desde febrero hasta diciembre, todos los días.",
                algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Ir a correr",
                        MesDesde = 2,
                        MesHasta = 12,
                        Frecuencia = Frecuencia.diaria
                    }
                )
            );
            Assert.True(algoritmo.EsValido);
            Assert.True(algoritmo.Errores == "");
        }
        [Test]
        public void Prueba6()
        {
            Assert.AreEqual
            ("Visitar al abuelo, en diciembre, cualquier día, mensual.",
                algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Visitar al abuelo",
                        MesDesde = 12,
                        MesHasta = 12,
                        Frecuencia=Frecuencia.mensual
                    }
                )
            );
            Assert.True(algoritmo.EsValido);
            Assert.True(algoritmo.Errores=="");
        }

        [Test]
        public void Prueba7()
        {
            Assert.AreEqual
            ("Pasear a la Ramira, desde enero hasta diciembre, todos los días.",
                algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Pasear a la Ramira",
                        MesDesde = 1,
                        MesHasta = 12,
                        Frecuencia = Frecuencia.diaria
                    }
                )
            );
            Assert.True(algoritmo.EsValido);
            Assert.True(algoritmo.Errores == "");
        }
        [Test]
        public void Prueba8()
        {
            Assert.AreEqual
            ("Hacer natación, en agosto, todos los sábados, semanal.",
                algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Hacer natación",
                        MesDesde = 8,
                        MesHasta = 8,
                        Frecuencia = Frecuencia.semanal,
                        Dia=6
                    }
                )
            );
            Assert.True(algoritmo.EsValido);
            Assert.True(algoritmo.Errores == "");
        }
        [Test]
        public void Prueba9()
        {
            Assert.AreEqual
            ("Ir al gimnasio, desde agosto hasta setiembre, cualquier día, semanal.",
                algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Ir al gimnasio",
                        MesDesde = 8,
                        MesHasta = 9,
                        Frecuencia = Frecuencia.semanal
                    }
                )
            );
            Assert.True(algoritmo.EsValido);
            Assert.True(algoritmo.Errores == "");
        }
        [Test]
        public void Prueba10()
        {
            Assert.AreEqual
            ("Matricularme en clases, desde enero hasta diciembre, cualquier día, semestral.",
                algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Matricularme en clases",
                        MesDesde = 1,
                        MesHasta = 12,
                        Frecuencia = Frecuencia.semestral

                    }
                )
            );
            Assert.True(algoritmo.EsValido);
            Assert.True(algoritmo.Errores == "");
        }
        [Test]
        public void Error_DiaEnFrecuenciaDiaria()
        {
            var rpta = algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Pagar el recibo de luz",
                        MesDesde = 1,
                        MesHasta = 12,
                        Frecuencia = Frecuencia.diaria,
                        Dia = 1
                    }
                );
            Assert.False(algoritmo.EsValido);
            Assert.True(algoritmo.Errores.Contains(ErrorMsges.DiaEnSemanal));
        }
        [Test]
        public void Error_DiaSemanalNoValido()
        {
            var rpta = algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Pagar el recibo de luz",
                        MesDesde = 1,
                        MesHasta = 12,
                        Frecuencia = Frecuencia.semanal,
                        Dia = 8
                    }
                );
            Assert.False(algoritmo.EsValido);
            Assert.True(algoritmo.Errores.Contains(ErrorMsges.DiaSemanaNoValido));
        }
        [Test]
        public void Error_DiaMesNoValido()
        {
            var rpta = algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Pagar el recibo de luz",
                        MesDesde = 1,
                        MesHasta = 12,
                        Frecuencia = Frecuencia.mensual,
                        Dia = 40
                    }
                );
            Assert.False(algoritmo.EsValido);
            Assert.True(algoritmo.Errores.Contains(ErrorMsges.DiaMesNoValido));
        }
        [Test]
        public void Error_MesDesdeNoValido()
        {
            var rpta = algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Pagar el recibo de luz",
                        MesDesde = 18,
                        MesHasta = 5,
                        Frecuencia = Frecuencia.mensual,
                        Dia = 10
                    }
                );
            Assert.False(algoritmo.EsValido);
            Assert.True(algoritmo.Errores.Contains(ErrorMsges.MesNoValido));
        }
        [Test]
        public void Error_MesHastaNoValido()
        {
            var rpta = algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Pagar el recibo de luz",
                        MesDesde = 4,
                        MesHasta = 20,
                        Frecuencia = Frecuencia.mensual,
                        Dia = 10
                    }
                );
            Assert.False(algoritmo.EsValido);
            Assert.True(algoritmo.Errores.Contains(ErrorMsges.MesNoValido));
        }
        [Test]
        public void Error_MesesInvalidos()
        {
            var rpta = algoritmo.GenerarFrase
                (
                    new Actividad
                    {
                        Descripcion = "Pagar el recibo de luz",
                        MesDesde = 2,
                        MesHasta = 1,
                        Frecuencia = Frecuencia.mensual,
                        Dia = 10
                    }
                );
            Assert.False(algoritmo.EsValido);
            Assert.True(algoritmo.Errores.Contains(ErrorMsges.MesesInvalidos));
        }
    }
}
