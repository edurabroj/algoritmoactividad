﻿using Rabanal.Eduardo.Final.Algoritmo;
using Rabanal.Eduardo.Final.AlgoritmoModelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rabanal.Eduardo.Final.Algoritmo
{
    public class AlgoritmoActividad
    {
        public bool EsValido { get; set; }
        public string Errores { get; set; }

        public string GenerarFrase (Actividad actividad)
        {
            EsValido = true;
            Errores = "";

            Validar(actividad);

            if (!EsValido)
                return "errores: " + Errores;

            string respuesta = "";
            Dictionary<int, string> dias = getDias();
            Dictionary<int, string> meses = getMeses();

            //actividad
            respuesta += actividad.Descripcion + ", ";
            //mes
            if(actividad.MesDesde==actividad.MesHasta)
            {
                respuesta += "en " + meses[actividad.MesDesde];
            }
            else
            {
                respuesta += "desde " + meses[actividad.MesDesde] + " hasta " + meses[actividad.MesHasta];
            }
            respuesta += ", ";
            //dia
            if(actividad.Frecuencia==Frecuencia.diaria)
            {
                respuesta += "todos los días.";
            }
            else
            {
                if (actividad.Dia == 0)
                {
                    respuesta += "cualquier día";
                }
                else
                {
                    respuesta += "todos los ";
                    if (actividad.Frecuencia == Frecuencia.semanal)
                    {
                        //dia semana
                        respuesta += dias[actividad.Dia];
                    }

                    else
                    {
                        if (actividad.Dia == 31)
                        {
                            respuesta += "fin de mes";
                        }
                        else
                        {
                            //dia de mes
                            respuesta += actividad.Dia;
                        }
                    }
                }
                respuesta += ", ";
                //frecuencia
                respuesta += actividad.Frecuencia.ToString() + ".";
            }

            return respuesta;

        }

        public Dictionary<int, string> getDias()
        {
            var dias = new Dictionary<int, string>();
            dias.Add(1, "lunes");
            dias.Add(2, "martes");
            dias.Add(3, "miercoles");
            dias.Add(4, "jueves");
            dias.Add(5, "viernes");
            dias.Add(6, "sábados");
            dias.Add(7, "domingos");
            return dias;
        }

        public Dictionary<int, string> getMeses()
        {
            var meses = new Dictionary<int, string>();
            meses.Add(1, "enero");
            meses.Add(2, "febrero");
            meses.Add(3, "marzo");
            meses.Add(4, "abril");
            meses.Add(5, "mayo");
            meses.Add(6, "junio");
            meses.Add(7, "julio");
            meses.Add(8, "agosto");
            meses.Add(9, "setiembre");
            meses.Add(10, "octubre");
            meses.Add(11, "noviembre");
            meses.Add(12, "diciembre");
            return meses;
        }

        public void Validar(Actividad actividad)
        {
            if (actividad.Frecuencia == Frecuencia.diaria)
            {
                if (actividad.Dia >= 1)
                {
                    EsValido = false;
                    Errores += ErrorMsges.DiaEnSemanal;
                }
            }
            if(actividad.Frecuencia==Frecuencia.semanal)
            {
                if(actividad.Dia<0 || actividad.Dia>7)
                {
                    EsValido = false;
                    Errores += ErrorMsges.DiaSemanaNoValido;
                }
            }
            if (actividad.Frecuencia == Frecuencia.mensual)
            {
                if (actividad.Dia < 0 || actividad.Dia > 31)
                {
                    EsValido = false;
                    Errores += ErrorMsges.DiaMesNoValido;
                }
            }
            if((actividad.MesDesde<1 || actividad.MesDesde>12) || (actividad.MesHasta < 1 || actividad.MesHasta > 12))
            {
                EsValido = false;
                Errores += ErrorMsges.MesNoValido;
            }
            if (actividad.MesDesde > actividad.MesHasta)
            {
                EsValido = false;
                Errores += ErrorMsges.MesesInvalidos;
            }
        }

        //public bool EsValido(Actividad actividad, string error)
        //{
        //    if(actividad.Frecuencia==Frecuencia.diaria && actividad.Dia>=0)
        //    {
        //        error = " dia";
        //        return false;
        //    }
        //    return true;
        //}
    }
}