﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rabanal.Eduardo.Final.Algoritmo
{
    public static class ErrorMsges
    {
        public static string DiaEnSemanal = "dia en semana.";
        public static string DiaSemanaNoValido = "dia de la semana no válido.";
        public static string DiaMesNoValido = "dia del mes no válido.";
        public static string MesNoValido = "mes no válido.";
        public static string MesesInvalidos = "mes desde debe ser anterior a mes hasta";
    }
}