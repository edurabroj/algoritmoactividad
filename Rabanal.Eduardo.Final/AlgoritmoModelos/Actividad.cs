﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rabanal.Eduardo.Final.AlgoritmoModelos
{
    public class Actividad
    {
        public Frecuencia Frecuencia { get; set; }
        public string Descripcion { get; set; }
        public int MesDesde { get; set; }
        public int MesHasta { get; set; }
        public int Dia { get; set; }
    }
}