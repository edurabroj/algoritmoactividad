﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Rabanal.Eduardo.Final.AlgoritmoModelos
{
    public enum Frecuencia
    {
        diaria,
        semanal,
        mensual,
        trimestral,
        semestral
    }
}