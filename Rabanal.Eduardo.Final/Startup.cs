﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Rabanal.Eduardo.Final.Startup))]
namespace Rabanal.Eduardo.Final
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
